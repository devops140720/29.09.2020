
import requests # requires installation: File > Settings > Project > Python interperter > + > requests > Install
import json

class Photo:
    def __init__(self, id, title, url, thumbnailUrl):
        self.id = id
        self.title = title
        self.url = url
        self.thumbnailUrl = thumbnailUrl
    def __str__(self):
        return f'Photo : id : {self.id}, title: {self.title}, url : {self.url}, thumbnail-url : {self.thumbnailUrl}'
    def __repr__(self):
        return f'Photo({self.id},{self.title}, {self.url},{self.thumbnailUrl})'
    def __eq__(self, other):
        if self.id == other.id:
            return True
        else:
            return False
    def __gt__(self, other):
        if self.id > other.id:
            return True
        else:
            return False
    def __lt__(self, other):
        if self.id < other.id:
            return True
        else:
            return False

class QuickPhoto:
    def __init__(self, d):
        self.__dict__ = d
    def __str__(self):
        result = "Quick photo :\n"
        for k, v in self.__dict__.items():
            result += k
            result += " : "
            result += str(v)
            result += '\n'
        return result
    def __eq__(self, other):
        if self.id == other.id:
            return True
        else:
            return False
    def __gt__(self, other):
        if self.id > other.id:
            return True
        else:
            return False
    def __lt__(self, other):
        if self.id < other.id:
            return True
        else:
            return False

photo1 = Photo(1, 'moshe', 'http://moshe.pic', 'http://moshe_thumb.pic')
photo2 = Photo(1, 'moshe', 'http://moshe.pic', 'http://moshe_thumb.pic')
photo3 = Photo(3, 'danny', 'http://danny.pic', 'http://danny_thumb.pic')
print (f'photo1 == photo2 ? {photo1 == photo2}')
print(f'photo3 > photo1 ? {photo3 > photo1}')
print(f'photo1 > photo3 ? {photo1 > photo3}')
print(f'photo3 < photo1 ? {photo3 < photo1}')
print(f'photo1 < photo3 ? {photo1 < photo3}')
print(photo1)
print(photo2.__repr__())

# create some photos from web service REST interent
# https://jsonplaceholder.typicode.com/photos
resp = requests.get('https://jsonplaceholder.typicode.com/photos/1')
print(resp.status_code)
print(resp.content)
one_photo_dict = json.loads(resp.content)
print(one_photo_dict)
one_photo_from_web = Photo(one_photo_dict['id'], one_photo_dict['title'], one_photo_dict['url'], one_photo_dict['thumbnailUrl'])

print('============ from web 1: ')
print(f'photo from web: {one_photo_from_web}')

resp = requests.get('https://jsonplaceholder.typicode.com/photos/2')
one_photo_dict = json.loads((resp.content))
second_photo_from_web = Photo(one_photo_dict['id'], one_photo_dict['title'], one_photo_dict['url'], one_photo_dict['thumbnailUrl'])
print('============ from web 2: ')
print(f'photo from web: {second_photo_from_web}')

resp = requests.get('https://jsonplaceholder.typicode.com/photos/2')
one_photo_dict = json.loads((resp.content))
second_second_photo_from_web = Photo(one_photo_dict['id'], one_photo_dict['title'], one_photo_dict['url'], one_photo_dict['thumbnailUrl'])
print('============ from web 2: ')
print(f'photo from web: {second_second_photo_from_web}')

print(f'second == second ? {second_photo_from_web == second_second_photo_from_web}')

quick_photo1 = QuickPhoto(one_photo_dict)
print(quick_photo1)